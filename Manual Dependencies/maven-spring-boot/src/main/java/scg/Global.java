package scg;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class Global {

	private final String CONFIG_PATH = "config/config.json";
	private final String LOG_PATH = "../log";

	public boolean load() {

		JSONObject root = alisa.json.Parser.loadObject(CONFIG_PATH);
		if (root == null) {
			this._error = "parsing error";
			return false;
		}

		// version (1.0)
		try {
			String version = root.getString("version");
			if (!version.equals("1.0")) {
				this._error = "incorrect version (" + version + ")";
				return false;
			}
		} 
		catch (JSONException ex) {
			this._error = "version";
			return false;
		}

		// port (int)
		try { this._port = root.getInt("port"); } 
		catch (JSONException ex) {
			this._error = "port";
			return false;
		}

		return true;
	}

	public void start() {

		this.log("-------------------------------------------------------------------------------");
		this.log(About.NAME + " " + About.VERSION + " (" + About.UPDATE + ")");
	}

	public boolean log(String content) {

		alisa.DateTime now = new alisa.DateTime();
		int year = now.getYear();
		int month = now.getMonth();
		int day = now.getDay();

		String path = LOG_PATH + "/" + year;
		if (month < 10) { path += "/0" + month; } 
		else { path += "/" + month; }

		if (day < 10) { path += "/0" + day; } 
		else { path += "/" + day; }

		File file = new File(path);
		if (!file.exists()) { file.mkdirs(); }

		String log_file_path = file + "/" + About.NAME + ".txt";

		// write log        
		try (FileWriter writer = new FileWriter(log_file_path, true)) { // true = Append, false = Overwrite
			writer.write(now.toString() + "\t\t" + content + "\r\n");
		} 
		catch (IOException ex) { return false; }

		// output to display
		System.out.println(now.toString() + "\t\t" + content);

		return true;
	}

	// ---------------- Attributes --------------------    
	public int getPort() {
		return this._port;
	}
	private int _port = -1;

	public String getError() {
		return this._error;
	}
	private String _error = "";
}
