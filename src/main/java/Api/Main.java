package Api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.json.*;

//import static com.example.ApiForBOL.SpringBoot.CallSpringBoot;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;


@SpringBootApplication(exclude = {
    MongoAutoConfiguration.class, MongoDataAutoConfiguration.class
})

public class Main {
      public static Global global = new Global();

        public static void main(String[] args) throws InterruptedException, IOException {
             
                // call load function
                if (!global.load()) {
                        System.out.println("Loading Error: " + global.getError());
                        return;
	}
                
                // start Spring Boot
	SpringApplication app = new SpringApplication(Main.class);
	Map<String, Object> properties = new HashMap<>();
	properties.put("server.port", global.getPort());
	app.setDefaultProperties(properties);
	app.run(args);
                
                // start
	global.start();
        
        }
}
